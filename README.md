# Smart Middleware Platform (Simpl) for Cloud-to-Edge Federations and Data Spaces

## Overview
This is a group where we develop the [Simpl project](https://digital-strategy.ec.europa.eu/en/policies/simpl). Simpl is Smart Middleware Platform designed to federate data, software, and infrastructure across the European Union, fostering collaboration between public and private sectors, including research institutions across various industries.

## Simpl Components

### [Simpl-Open](https://code.europa.eu/simpl/simpl-open):
* An open-source software stack powering data spaces and cloud-to-edge federation initiatives.

### [Simpl-Labs](https://code.europa.eu/simpl/simpl-labs):
* A testing environment for data spaces to assess interoperability with Simpl.
* Allows sectoral data spaces in early stages to experiment with deployment, maintenance, and support.
* Enables mature data spaces to assess interoperability with Simpl-Open.

### Simpl-Live:
* Deployed instances of the Simpl-Open software stack for specific sectoral data spaces.
* Involves active management participation from the European Commission.

## Community Engagement & Contributions
Simpl fosters a collaborative environment across the EU by enabling secure and efficient data-sharing ecosystems. By actively supporting open collaboration and providing clear onboarding pathways, Simpl strengthens participation in the European data space infrastructure. For more information, have a look at the [Simpl Contributions](https://code.europa.eu/simpl/simpl-contributions) group.
